                                                                                                                                                                  
package game;                                                                                                                                                      
import java.util.*;                                                                                                                                                                                                                                                                                                     
public class game1                                                                                                                                            
{                                                                                                                                                                 
	public static  void main(String args[])                                                                                                                       
	{                                                                                                                                                             
		Scanner input = new Scanner(System.in);
		String[][] qi = new String[6][7];
		String nuu = "|   ";
		String play1 = "| O ";
		String play2 = "| X ";
		int count = 0;//誰下子及下了多少子的判斷計數器
		//初始化棋子數組
		for(int i = 0;i < qi.length;i++)
			for(int j = 0;j < qi[0].length;j++)
				qi[i][j] = nuu;
		//畫出原始棋盤
		System.out.println("-----------------------------");
		for(int i = qi.length - 1;i >= 0;i--){
			for(int j = 0;j < qi[0].length;j++)
				System.out.print(qi[i][j]);
			System.out.println("|");
			System.out.println("-----------------------------");
		}
		for(int i = 0;i < qi[0].length;i++)
			System.out.print("| "+(i+1)+" ");
		System.out.println("|");
		
		int row = 0,column = 0;//行列下標初始化
		//下棋循環開始
		while (true) {
			row = 0;
			column = 0;
			if (count % 2 == 0) {//play1下棋
				System.out.print("\n play1請輸入(1~7):");
				while (true) {//play1下棋循環開始
					column = input.nextInt() - 1;
					if (column >= 0 && column <= 6) {// 输入合法進行下棋
						for (row = 0; row < qi.length; row++) {
							if (qi[row][column] == nuu) {
								qi[row][column] = play1;
								break;
							}
						}
						if (row == qi.length)//該行棋子已滿，重新输入
							System.out.print("\n 該行棋子已滿請重新輸入 : ");
						else//棋子没滿，下棋结束
							break;
					} 
					else// 輸入不合法，重新下棋
						System.out.print("\n 請輸入正確數值(1~7):");
				}//play1下棋循環结束
			}//if(count % 2 == 0)
			else{//play2下棋
				System.out.print("\n play2請輸入(1~7):");
				while (true) {//play2下棋循環開始
					column = input.nextInt() - 1;
					if (column >= 0 && column <= 6) {// 输入合法進行下棋
						for (row = 0; row < qi.length; row++) {
							if (qi[row][column] == nuu) {
								qi[row][column] = play2;
								break;
							}
						}
						if (row == qi.length)//該行棋子已滿，重新輸入
							System.out.print("\n 該行棋子已滿請重新輸入 : ");
						else//棋子没滿，下棋结束
							break;
					} 
					else// 輸入不合法，重新下棋
						System.out.print("\n 請輸入正確數值(1~7): ");
				}//Play2下棋循環结束
			}//if(count % 2 != 0)
			
			
			//畫出棋盤
			System.out.println("-----------------------------");
			for(int i = qi.length - 1;i >= 0;i--){
				for(int j = 0;j < qi[0].length;j++)
					System.out.print(qi[i][j]);
				System.out.println("|");
				System.out.println("-----------------------------");
			}
			for(int i = 0;i < qi[0].length;i++)
				System.out.print("| "+(i+1)+" ");
			System.out.println("|");
		
			//判斷輸贏
			boolean flagForYWin = false;
			boolean flagForRWin = false;
			
			//只需對當前下棋的周圍情况進行判斷来决定棋局结果
			if (count % 2 == 0) {
				//列檢測開始	
				if (column <= 3) {
					for (int jj = 0; jj <= column; jj++)
						if (qi[row][jj] == play1 
						        && qi[row][jj + 1] == play1
								&& qi[row][jj + 2] == play1
								&& qi[row][jj + 3] == play1) {
							flagForYWin = true;
							break;
						}
				} 
				else {
					for (int jj = column - 3; jj <= 3; jj++)
						if (qi[row][jj] == play1 
						        && qi[row][jj + 1] == play1
								&& qi[row][jj + 2] == play1
								&& qi[row][jj + 3] == play1) {
							flagForYWin = true;
							break;
						}
				}
				if (flagForYWin) {
					System.out.println("play1贏得比賽!");
					break;
				}
				//斜線檢測
				if(row >= 3){
					if(column < 3){
						if (qi[row][column] == play1 
								&& qi[row - 1][column + 1] == play1
								&& qi[row - 2][column + 2] == play1
								&& qi[row - 3][column + 3] == play1)
							flagForYWin = true;
					}
					else if(column > 3){
						if (qi[row][column] == play1 
								&& qi[row - 1][column - 1] == play1
								&& qi[row - 2][column - 2] == play1
								&& qi[row - 3][column - 3] == play1)
							flagForYWin = true;
					}
					else{
						if ((qi[row][column] == play1 
								&& qi[row - 1][column + 1] == play1
								&& qi[row - 2][column + 2] == play1
								&& qi[row - 3][column + 3] == play1) 
								|| (qi[row][column] == play1 
								&& qi[row - 1][column - 1] == play1	
								&& qi[row - 2][column - 2] == play1		
								&& qi[row - 3][column - 3] == play1))
							flagForYWin = true;
					}
				}
				if (flagForYWin) {
					System.out.println("play1贏得比賽!");
					break;
				}
				//行檢測開始
				if (row >= 3) {
					if (qi[row][column] == play1 
							&& qi[row - 1][column] == play1
							&& qi[row - 2][column] == play1
							&& qi[row - 3][column] == play1)
						flagForYWin = true;
				}
				if (flagForYWin) {
					System.out.println("play1贏得比賽!");
					break;
				}
				
			}//play1是否贏?
			else{//play2贏
				//列檢測開始	
				if (column <= 3) {
					for (int jj = 0; jj <= column; jj++)
						if (qi[row][jj] == play2 
						        && qi[row][jj + 1] == play2
								&& qi[row][jj + 2] == play2
								&& qi[row][jj + 3] == play2) {
							flagForRWin = true;
							break;
						}
				} 
				else {
					for (int jj = column - 3; jj <= 3; jj++)
						if (qi[row][jj] == play2 
						        && qi[row][jj + 1] == play2
								&& qi[row][jj + 2] == play2
								&& qi[row][jj + 3] == play2) {
							flagForRWin = true;
							break;
						}
				}
				if (flagForRWin) {
					System.out.println("play2贏得比賽!");
					break;
				}
				//斜線檢測
				if(row > 3){
					if(column < 3){
						if (qi[row][column] == play2 
								&& qi[row - 1][column + 1] == play2
								&& qi[row - 2][column + 2] == play2
								&& qi[row - 3][column + 3] == play2)
							flagForRWin = true;
					}
					else if(column > 3){
						if (qi[row][column] == play2 
								&& qi[row - 1][column - 1] == play2
								&& qi[row - 2][column - 2] == play2
								&& qi[row - 3][column - 3] == play2)
							flagForRWin = true;
					}
					else{
						if ((qi[row][column] == play2 
								&& qi[row - 1][column + 1] == play2
								&& qi[row - 2][column + 2] == play2
								&& qi[row - 3][column + 3] == play2) 
								|| (qi[row][column] == play2 
								&& qi[row - 1][column - 1] == play2	
								&& qi[row - 2][column - 2] == play2
								&& qi[row - 3][column - 3] == play2))
							flagForRWin = true;
					}
				}
				if (flagForRWin) {
					System.out.println("play2贏得比賽!");
					break;
				}
				
				//行檢測开始
				if (row >= 3) {
					if (qi[row][column] == play2 
							&& qi[row - 1][column] == play2
							&& qi[row - 2][column] == play2
							&& qi[row - 3][column] == play2)
						flagForRWin = true;
				}
				if (flagForRWin) {
					System.out.println("play2贏得比賽!");
					break;
				}
				
			}
			
			count++;//棋子數加1，判斷目前玩家
			
			//棋盤已滿，平局
			if(count == 6*7){
				System.out.println("棋盤棋子已經下滿，這是平局！");
				break;
			}
		}//下棋循環结束
	}
}
                                                                                                   