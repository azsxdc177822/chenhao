package Chen;
import java.util.*;
class Account implements Comparable<Account> {
    protected String name;
    private int balance;

    Account(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Account(%s,%d)", name, balance);
    }

    @Override
    public int compareTo(Account other) {
        return this.balance - other.balance;
    }
}
public class test3 {

	public static void main(String[] args) {
		   List<Account> accounts = Arrays.asList(
	                new Account("Mary", 30),
	                new Account("Gary", 78),
	                new Account("Tery", 66),
	                new Account("Ben", 1200),
	                new Account("Alice", 18)
	        );
	        Collections.sort(accounts);
	        System.out.println("money�Ƨ�:"+accounts);
	        accounts.sort(Comparator.comparing(acc->acc.name));
	        System.out.println("name�Ƨ�:"+accounts);
	}

}
