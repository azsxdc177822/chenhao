package Chen;


public class test2 {
	public static abstract class Animal{
		String name;
		public Animal() {
			name ="animal";
		}
		public void setName(String n) {
			name=n;
		}
		public abstract void move();
		public abstract void sound();
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal dog=new Animal() {
			@Override
			public void move() {
				System.out.println("Move:walk");
			}

			@Override
			public void sound() {
				System.out.println("Sound:rrrrr");
			}
		};
		dog.setName("dog");
		dog.move();
		dog.sound();
	}

}
